from django import forms
from .models import EmailId

class EmailForm(forms.ModelForm):
	class Meta:
		model=EmailId
		fields=('email_id',)
		widgets={
			'email_id':forms.TextInput(
					attrs={'placeholder':'Your Email Address','type':'email','class':'form-control'}
				)
		}
		

		