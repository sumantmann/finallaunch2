$(function(){
    $form2=$('.form2');
    $name_error=$('#name-error');
    $email_error=$('#email-error');
    $city_error=$('#city-error');
    $number_error=$('#number-error');
    $inputStr=$('#id_person_email');

    $form2.submit(function(e){
        
        flag=0;
        //name validation
        if($('#id_person_name').val()===''){
            $name_error.html('Please enter your name');
            flag=1;
        }
        else{
            $name_error.html('');
        } 
        //email validation
        if($('#id_person_email').val()===''){
            $email_error.html('Please enter your email');
            flag=1;

        }
        if($inputStr.val().length <6 || $inputStr.val().indexOf('<script>')>=0 || $inputStr.val().indexOf('@')<0 || $inputStr.val().indexOf('.')<0 ){
            $email_error.html('Please enter a valid email');
            flag=1;
        }
        else{
            $email_error.html('');
        }
        //city validation
        if ($('#id_person_city').val()==='') {
            $city_error.html('Please enter your city');
            flag=1;
        }
        else{
            $city_error.html('');
        }
        //phone number validation
        if($('#id_phone_number').val()===''){
            $number_error.html('Please enter your phone number');
            flag=1;
        }
        if($('#id_phone_number').val().length<8){
           $number_error.html('Please enter your phone number');  
           flag=1;          
        }
        else{
            $number_error.html('');
        }

        if(flag===1){
            e.preventDefault();
            return false;
        }
        else if(flag==0){
            console.log("flag 0");
            $("#myModal").modal();
        }
        
    });
    $('.form-control').focus(function(){
        $(this).parent().find('.errors').html('');
    });

});