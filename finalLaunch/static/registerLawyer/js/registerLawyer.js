$(document).ready(function(){
  var $form=$('#register-form');
  var $year=$('#id_date_of_birth_year');
  var $month=$('#id_date_of_birth_month');
  var $day=$('#id_date_of_birth_day');
  var $number_error=$('.number-error');
  var $city_error=$('.city-error');
  var $fname_error=$('.fname-error');
  var $lname_error=$('.lname-error');
  var $email_error=$('.email-error');
  var inputStr=$('#id_email').val();   
	$form.submit(function(e){
		var flag=0;
		if( $('#id_city_name').val()===''){
			$city_error.html('Please enter your city name');
			flag=1;
		}
		else{
			$city_error.html('');
		}
		
		//first name validation
		if($('#id_first_name').val()===''){
  			$fname_error.html('Please enter First Name');
  			flag=1;
  		}	
  		else{
  			$fname_error.html('');

  		}
		
		//last name validation
		if($('#id_last_name').val()===''){
  			$lname_error.html('Please enter Last Name');
  			flag=1;
  			
		}
		else{
  			$lname_error.html('');

		}
		//email validation
    inputStr=$('#id_email').val();
		if($('#id_email').val()===''){
  			$email_error.html('Please enter email address');
  			flag=1;
		}
    if($('#id_email').val().length <6 || $('#id_email').val().indexOf('@')<0 || $('#id_email').val().indexOf('.')<0){
        $email_error.html('Please enter valid email address');
        flag=1;
    }
		else{
  			$email_error.html('');

		}
		//phone number validation
		if($('#id_phone_number').val().length===0){
  			$number_error.html('Please enter phone number');
  			flag=1;
  		}
  	else if ($('#id_phone_number').val().length<10 && $('#id_phone_number').val().length>0){
  			$number_error.html('Please enter a valid number');
  			flag=1;
  		}
  		else{
  			$number_error.html('');

  		}
  		
  		//date of birth validatoin
  		if ($year.val()==='-1' || $month.val()==='-1'|| $day.val()==='-1' ){
  			$('.dob-error').html('Please enter your date of birth');

  		}
  		else{
  			$('.dob-error').html('');

  		}
      //flag test
  		if(flag===1){
  			e.preventDefault();
  			return false;
  		}
      else{
        window.location.href("http://127.0.0.1:8000/lawyer-registration/thank-you/");
      }
	});
	$('.form-control').focus(function(){
		$(this).parent().find('.errors').html('');
	});
	$year.prepend('<option class="show-field" selected="selected" value="-1">Year</option>');
	$month.prepend('<option class="show-field" selected="selected" value="-1">Month</option>');
	$day.prepend('<option class="show-field" selected="selected" value="-1">Day</option>');


});	

