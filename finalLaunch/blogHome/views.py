from django.shortcuts import render
from django.http import HttpResponse,HttpResponseRedirect,Http404
from .models import InterestedPeople,Article
from .forms import ContactForm



#All Articles.....................................................................
def allArticle():
	all_articles_obj=Article.objects.all()
	return all_articles_obj
	
#All Slugs........................................................................
def allSlug():
	all_articles_obj=Article.objects.all()
	all_articles_slugs=[x.article_slug for x in all_articles_obj]
	return all_articles_slugs
#Blog Home page handler............................................................
def ldHomeView(request):
	if request.method=='POST':
		form=ContactForm(data=request.POST)
		if form.is_valid():
			interestedPeople_obj=form.save()
			#interestedPeople_obj.save()
			popUp=1
			return render(request,'blogHome/blogHome.html',{'form':form,'popUp':popUp})
		else:
			print(form.errors)	
	else:
		form=ContactForm()

	all_articles=allArticle()
	latest_articles=all_articles[:5]	
	return render(request,'blogHome/blogHome.html',{'form':form,'latest_articles':latest_articles})			

#Single Blog page Handler..........................................................
def singleArticleView(request,slug):
	middle=None
	if request.method=='GET':

		all_articles_slugs=allSlug()
		if slug in all_articles_slugs:
			current_object=getCurrentObj(slug)
			
			if current_object.middle_image == None:
				middle=0
			else:
				middle=1	
		else:
			return HttpResponse('404 error')
	print(request.get_full_path())		
	return render(request,'singleArticle/singleArticle.html',{'current_object':current_object,'middle':middle,'current_path': request.get_full_path()})


def getCurrentObj(slug):
	all_slug=allSlug()
	index_current_slug=all_slug.index(slug)
	all_articles_obj=allArticle()
	current_object=all_articles_obj[index_current_slug]
	return current_object


#All Blog Page Handler..............................................................	
def allArticleView(request):
	all_articles_obj=allArticle()
	latest_articles3=all_articles_obj[:9]
	return render(request,'allArticles/allArticles.html',{'latest_articles':latest_articles3})


#404 Error Page Handler.............................................................
def handler404(request):
	return render(request,'404.html')	