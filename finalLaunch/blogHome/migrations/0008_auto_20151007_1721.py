# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('blogHome', '0007_auto_20151007_1636'),
    ]

    operations = [
        migrations.AlterField(
            model_name='article',
            name='middle_image',
            field=models.ImageField(null=True, blank=True, upload_to='blogHome/articleImages'),
        ),
    ]
