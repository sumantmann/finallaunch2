# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import tinymce.models


class Migration(migrations.Migration):

    dependencies = [
        ('blogHome', '0004_auto_20150912_1657'),
    ]

    operations = [
        migrations.AlterField(
            model_name='article',
            name='article_body',
            field=tinymce.models.HTMLField(verbose_name='Enter the body of article'),
        ),
    ]
