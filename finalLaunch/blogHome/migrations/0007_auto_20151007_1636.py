# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('blogHome', '0006_auto_20150928_1813'),
    ]

    operations = [
        migrations.AlterField(
            model_name='article',
            name='middle_image',
            field=models.ImageField(blank=True, upload_to='blogHome/articleImages'),
        ),
    ]
