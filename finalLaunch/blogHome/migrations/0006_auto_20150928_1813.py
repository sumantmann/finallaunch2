# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('blogHome', '0005_auto_20150922_1132'),
    ]

    operations = [
        migrations.AddField(
            model_name='article',
            name='article_body1',
            field=models.TextField(verbose_name='Enter body2 of article', null=True, blank=True),
        ),
        migrations.AddField(
            model_name='article',
            name='middle_image',
            field=models.ImageField(upload_to='blogHome/articleImages', null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='article',
            name='article_body',
            field=models.TextField(verbose_name='Enter body1 of article'),
        ),
    ]
