from django.contrib import admin
from .models import Article,InterestedPeople
# Register your models here.

class ArticleAdmin(admin.ModelAdmin):
	fields=['article_title','article_slug','article_body','article_body1',
			'middle_image','cover_image','article_author','pub_date']

	prepopulated_fields={'article_slug':["article_title"]}
	class Meta:
		model=Article			




admin.site.register(Article,ArticleAdmin)
admin.site.register(InterestedPeople)



















