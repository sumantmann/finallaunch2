from datetime import datetime
from django.core.urlresolvers import reverse
from django.db.models.signals import post_save
from django.db import models
from django.core.mail import send_mail


HTML_MESSAGE='<body style="vertical-align:middle;width:100%;"> \
	<table border="0" cellpadding="0" cellpadding="0" style="color: #333333;border-collapse:collapse;margin:0 auto;width:100%;">\
	 <tr style="width:100%; height:62px;text-align:center;"> <td style="text-align:center;"> <img style="width:100%;" \
	 src="https://s3-ap-northeast-1.amazonaws.com/teamlawyered/lawyered_newsletter_blogger_banner1.png"> </td> </tr> \
	 <tr style="text-align:center;"> <td style="letter-spacing:3px;padding-top:45px;color:#333333;"> <h3>THANK YOU FOR REGISTERING!\
	 </h3> </td> </tr> <tr style="text-align:center;"> <td style="text-align:center;padding-bottom:45px;"> <p style="color:#eb6a2b;font-style:italic;letter-spacing:1px;">We invite you to voice your opinion.<br> Share information,tips and advice to help readers tackle legal efficiency. </p> </td> </tr> <tr style="text-align:center;"> <td style="padding-bottom:20px;"> <p style="letter-spacing:1.2px;color:black"> Email us your story at <span style="color:#eb6a2b;">legaldisrupt@lawyered.in </span>along with a short<br> discription about yourself. </p> <p style="color:black;letter-spacing:1px;">We will get in touch with you at the earliest!</p> </td> </tr> <tr style="width:100%; height:62px;text-align:center;"> <td style="text-align:center;"> <img style="width:100%;" src="https://s3-ap-northeast-1.amazonaws.com/teamlawyered/lawyered_newsletter_blogger_banner2.png"> </td> </tr> <tr style="text-align:center;"> <td style="text-align:center;padding-top:20px;"> <a style="display:inline-block;" href="https://www.facebook.com/lawyered.in" target="_blank"> <img style="height:30px; width:30px;" src="https://s3-ap-northeast-1.amazonaws.com/teamlawyered/facebook_.png"> </a> <a style="display:inline-block;" href="https://plus.google.com/108952108231921483693/about" target="_blank"> <img style="height:30px; width:30px;" src="https://s3-ap-northeast-1.amazonaws.com/teamlawyered/google_.png"> </a> <a style="display:inline-block;" href="https://www.linkedin.com/company/lawyered" target="_blank"> <img style="height:30px; width:30px;" src="https://s3-ap-northeast-1.amazonaws.com/teamlawyered/linkedin_.png"> </a> <a style="display:inline-block;" href="https://twitter.com/LawyeredTweets" target="_blank"> <img style="height:30px; width:30px;" src="https://s3-ap-northeast-1.amazonaws.com/teamlawyered/twitter_.png"> </a> </td> </tr> </table> </body>'

	 
class Article(models.Model):
	article_title=models.CharField('Enter the title of your article',max_length=200,unique=True)
	article_slug=models.SlugField(max_length=100,unique=True)
	article_body=models.TextField('Enter body1 of article',)
	article_body1=models.TextField('Enter body2 of article',null=True,blank=True)
	middle_image=models.ImageField(upload_to='blogHome/articleImages',null=True,blank=True)
	cover_image=models.ImageField(upload_to='blogHone/articleImages')
	article_author=models.CharField('Enter author name',max_length=100)
	pub_date=models.DateTimeField('date published',default=datetime.now,blank=True)
	def get_absolute_url(self):
		return reverse('blogHome.views.',args=[str(self.article_slug)])
	def __str__(self):
		return self.article_title		

class InterestedPeople(models.Model):
	person_name=models.CharField(max_length=100,blank=False)
	person_email=models.EmailField(blank=False)
	person_city=models.CharField(max_length=30)
	phone_number=models.CharField(max_length=14)
	def __str__(self):
		return self.person_name
		
def send_email_to_interested_people(sender,instance,created,*args,**kwargs):
	if instance.person_email:
		send_mail('Welcome to Lawyered','','admin@lawyered.in',[instance.person_email],fail_silently=True,html_message=HTML_MESSAGE)
	else:
		pass	


post_save.connect(send_email_to_interested_people,sender=InterestedPeople)
