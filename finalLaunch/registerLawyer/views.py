from django.shortcuts import render
from django.http import HttpResponseRedirect,HttpResponse
from .forms import  lawyerRegisterForm
from .models import LawyerRegisterDetail

# Create your views here.
def lawyerRegisterView(request):
	if request.method=='POST':
		reg_form=lawyerRegisterForm(request.POST)
		if reg_form.is_valid():
			print("xxxxxxxxxxxxxxxxxxxxxx")
			reg_obj=reg_form.save()
			return HttpResponseRedirect('/lawyer-registration/thank-you')	
		else:
			print(reg_form.errors)
	if request.method=='GET':
		reg_form=lawyerRegisterForm()
	return render(request,'registerLawyer/registerBlock.html',{'reg_form':reg_form})

def lawyerThankView(request):
	if request.method=='GET':
		return render(request,'registerLawyer/registerThankyou.html')
