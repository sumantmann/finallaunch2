from django import forms
from django.forms import extras
from .models import LawyerRegisterDetail
GENDER_CHOICES=(('Male','Male'),('Female','Female'),)
class lawyerRegisterForm(forms.ModelForm):
	gender=forms.ChoiceField(choices=GENDER_CHOICES,initial=GENDER_CHOICES[0][0], widget=forms.RadioSelect())
	class Meta:
		model = LawyerRegisterDetail
		fields='__all__'
		widgets={
			'first_name':forms.TextInput(
					attrs={'placeholder':'First name','type':'text','class':'form-control','name':'fname',
						'required autocomplete':'given-name'}
				),
			'last_name':forms.TextInput(
					attrs={'placeholder':'Last name','type':'text','class':'form-control','name':'lname',
						'required autocomplete':'family-name'}
				),
			'city_name':forms.TextInput(
					attrs={'placeholder':'Choose your city','type':'text','list':'city-list','class':'form-control'}
				),
			'company_name':forms.TextInput(
					attrs={'placeholder':'Enter company name','type':'text','class':'form-control'}
				),
			'date_of_birth':extras.SelectDateWidget(
					attrs={'class':'form-control date-picker required'},years=range(1905,2016)
				),
			'email':forms.EmailInput(
					attrs={'placeholder':'Enter your email address','type':'email','class':'form-control',
						'required autocomplete':'email'}
				),
			'phone_number':forms.NumberInput(
					attrs={'placeholder':'Enter your 10-digit mobile number','type':'tel','class':'form-control',
							'required autocomplete':'tel'
							,'required pattern':'^(?:(?:\+|0{0,2})91(\s*[\-]\s*)?|[0]?)?[789]\d{9}$'}
				),
			'state_bar_no':forms.TextInput(
					attrs={'placeholder':'State Bar where you originally enrolled','type':'text','class':'form-control'}
				),
			'bar_council_no':forms.TextInput(
					attrs={'placeholder':'Bar Council number','type':'text','class':'form-control'}
				)
			

		}
		exclude=['register_time']