from datetime import datetime
from django.db import models
from django.core.mail import send_mail
from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string


HTML_MESSAGE='<body style="vertical-align:middle;"> <table border="0" cellpadding="0" cellpadding="0" style="color: #333333;border-collapse:collapse;margin:0 auto;"> <tr style="width:100%; height:62px;text-align:center;"> <td style="text-align:center;"> <img style="width:100%;" src="https://s3-ap-northeast-1.amazonaws.com/teamlawyered/lawyered_newsletter_user_header.png"> </td> </tr> <tr style="text-align:center;"> <td style="letter-spacing:3px;"> <h3>THANK YOU FOR SUBSCRIBING!</h3> </td> </tr> <tr style="text-align:center;"> <td> <p>We Understand that you work really hard to turn your idea into a successful buisness.While doing so,the biggest mistake made by entrepreneurs is ignoring the legal compliances.Law is not a need,rather a necesity and cannot be overlooked. </p> </td> </tr> <tr style="width:100%; height:270px; padding-bottom:30px;" > <td style="text-align:center;"> <img style="width:100%;" src="https://s3-ap-northeast-1.amazonaws.com/teamlawyered/newsletter_user_info.png"> </td> </tr> <tr > <td style="padding-top:30px;text-align:center;"> <h4 style="font-style:italic;color:#eb6a2b;">We are here to make your life easier.We understand law and its complexity</h4> </td> </tr> <tr> <td style="text-align:center;padding-bottom:80px;"> <p>We help you connect with professional legal advisors and understand legal compliances for work</p> </td> </tr> <tr> <td style="height:35px;padding-bottom:40px;"> <img style="width:100%;" src="https://s3-ap-northeast-1.amazonaws.com/teamlawyered/lawyered_newsletter_user_fastersmater.png"> </td> </tr> <tr> <td style="text-align:center;padding-bottom:30px;"> <p style="color:#41dbeb; font-style:italic;">We are currently working on our platform to give<br> you the best possible interface</p> </td> </tr> <tr> <td style="text-align:center;padding-bottom:30px;"> <a href="https://www.facebook.com/lawyered.in" target="_blank"><img style="display:inline-block;" src="https://s3-ap-northeast-1.amazonaws.com/teamlawyered/facebook.png"height="30" width="30"></a> <a href=""target="_blank"><img style="display:inline-block;" src="https://s3-ap-northeast-1.amazonaws.com/teamlawyered/goog.png"height="30" width="30"></a> <a href="https://www.linkedin.com/company/lawyered"target="_blank"><img style="display:inline-block;" src="https://s3-ap-northeast-1.amazonaws.com/teamlawyered/likd.png"height="30" width="30"></a> <a href="https://twitter.com/LawyeredTweets"target="_blank"><img style="display:inline-block;"src="https://s3-ap-northeast-1.amazonaws.com/teamlawyered/twee.png"height="30" width="30"></a> </td> </tr> <tr> <td style="text-align:center;text-align:center;padding-bottom:30px;"> <div style="background-color:#333333;width:50px;margin:0 auto;height:1px;"></div> </td> </tr> <tr style="width:100%; height:10px;text-align:center;"> <td style="text-align:center;"> <img style="width:100%;" src="https://s3-ap-northeast-1.amazonaws.com/teamlawyered/end+line.png"height="10"> </td> </tr> </table> </body>'

# Create your models here.
class LawyerRegisterDetail(models.Model):
	GENDER_CHOICES=(('Male','Male'),('Female','Female'),)
	first_name=models.CharField(max_length=100)
	last_name=models.CharField(max_length=100)
	city_name=models.CharField(max_length=100)
	company_name=models.CharField(max_length=200,blank=True)
	date_of_birth=models.DateField()
	gender=models.CharField(choices=GENDER_CHOICES,default=GENDER_CHOICES[0][0],max_length=6)
	email=models.EmailField()
	phone_number=models.CharField(max_length=20)
	state_bar_no=models.CharField(blank=True,max_length=50)
	bar_council_no=models.CharField(blank=True,max_length=50)
	#terms_conditions=models.BooleanField(default=False)
	register_time=models.DateTimeField(default=datetime.now,blank=True)
	def __str__(self):
		return self.email + " registered at " +self.register_time.strftime("%Y-%m-%d")

	def save(self):
		if self.email:
			send_mail('Welcome to Lawyered','','admin@lawyered.in',[self.email],fail_silently=True,html_message=HTML_MESSAGE)
		super(LawyerRegisterDetail,self).save()	
